FROM openjdk:15
LABEL maintainer="groupeDevOps"
COPY target/devops-workshop-0.1.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]
EXPOSE 80